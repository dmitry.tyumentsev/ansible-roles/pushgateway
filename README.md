Pushgateway role
=========

Installs [Pushgateway](https://github.com/prometheus/pushgateway) on Fedora, Debian/Ubuntu servers.

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.pushgateway
```
